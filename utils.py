from numpy.random import seed
seed(2019)
from tensorflow import set_random_seed
set_random_seed(2019)  
from keras import backend as K
from keras import initializers
import keras
from keras.models import Model
from keras.layers import TimeDistributed,Multiply,Add,Activation,Subtract
from keras.layers.core import Layer
from keras.models import Sequential
from keras.layers import Dense, LSTM,GRU, Dropout, Bidirectional,SimpleRNN
from keras.layers import Conv1D, Conv2D, MaxPooling1D,Input,Reshape,Flatten,GlobalMaxPooling1D
from keras.layers.embeddings import Embedding
from keras.callbacks import ModelCheckpoint, EarlyStopping
from keras.optimizers import Adam,RMSprop
from keras.constraints import maxnorm
from keras.regularizers import l1,l2,l1_l2
from keras import backend as k
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from attention import *
import matplotlib.pyplot as plt
import numpy as np
seed1 = 2019
def pearson_r(y_true, y_pred):
    x = y_true
    y = y_pred
    mx = np.mean(x, axis=0)
    print('mx',mx)
    my = np.mean(y, axis=0)
    print('my',my)
    xm, ym = x - mx, y - my
    print('xm, ym',xm,ym)
    r_num = np.sum(xm * ym)
    x_square_sum = np.sum(xm * xm)
    y_square_sum = np.sum(ym * ym)
    r_den = np.sqrt(x_square_sum * y_square_sum)
    r = r_num / r_den
    return np.mean(r)

def two_attention (current_input): 
    shape_size = current_input.get_shape().as_list()  
    current_input_reshape = Reshape((shape_size[2],shape_size[1]))(current_input)
    featurea = Attention()(current_input)
    featureb = Attention()(current_input_reshape)
    all_features = keras.layers.concatenate([featurea,featureb], axis=-1)
    return all_features

def two_position (current_input): 
    shape_size = current_input.get_shape().as_list()  
    current_input_reshape = Reshape((shape_size[2],shape_size[1]))(current_input)
    featurea = Position_Embedding()(current_input)
    featureb = Position_Embedding()(current_input_reshape)
    s_size = featureb.get_shape().as_list()
    featurebb = Reshape((s_size[2],s_size[1]))(featureb)
    all_position_features = keras.layers.concatenate([featurea,featurebb], axis=-1)
    return all_position_features

def build_agnet(args,embedding_matrix,nb_words,rate,L2_value):
    print ('<<<<<<<<<<<<<<<<<<<<<<<<<<AGNet>>>>>>>>>>>>>>>>>>>>>>>>>>>')
    embedding_layer = Embedding(nb_words+1,
                        args.embedding_vector_length,
                        weights=[embedding_matrix],
                        input_length=args.MAX_LEN,
                        trainable=args.trainable)
    NB_FILTER = 64
    FILTER_LENGTH = [3,5,7]
    sequence_input = Input(shape=(args.MAX_LEN,), dtype='float32')
    embedded_sequences = embedding_layer(sequence_input)

    position_embeddings = Position_Embedding()(embedded_sequences)
    position_embeddings = Dropout(rate)(position_embeddings)
    position_vectors = two_attention(position_embeddings)

    embedded_sequences = Dropout(rate)(embedded_sequences)
    conv1 = Conv1D(NB_FILTER, FILTER_LENGTH[0], activation='relu', padding='same', kernel_regularizer=l2(L2_value), kernel_initializer = initializers.lecun_normal(seed1))(embedded_sequences)
    mp1 = MaxPooling1D(3)(conv1)
    conv2 = Conv1D(NB_FILTER, FILTER_LENGTH[1], activation='relu', padding='same', kernel_regularizer=l2(L2_value), kernel_initializer = initializers.lecun_normal(seed1))(embedded_sequences)
    mp2 = MaxPooling1D(3)(conv2)
    conv3 = Conv1D(NB_FILTER, FILTER_LENGTH[2], activation='relu', padding='same', kernel_regularizer=l2(L2_value), kernel_initializer = initializers.lecun_normal(seed1))(embedded_sequences)
    mp3 = MaxPooling1D(3)(conv3)
    con_feature = keras.layers.concatenate([mp1, mp2, mp3], axis=-1)
    #####################
    con_feature = Dropout(rate)(con_feature)
    ### adopt FILTER_LENGT
    gconv_feature1 = Conv1D(NB_FILTER*3, FILTER_LENGTH[0], activation='selu', padding='same',kernel_regularizer=l2(L2_value), kernel_initializer = initializers.lecun_normal(seed1))(con_feature) 
    gconv_feature2 = Conv1D(NB_FILTER*3, FILTER_LENGTH[0], activation='sigmoid', padding='same',kernel_regularizer=l2(L2_value), kernel_initializer = initializers.lecun_normal(seed1))(con_feature)
    gconv_feature = Multiply()([gconv_feature1, gconv_feature2])
    c_features1 = MaxPooling1D(3)(gconv_feature)

    c_features = Dropout(rate)(c_features1)
    ### adopt GRU units
    g_features1 = Bidirectional(GRU(128,return_sequences=True, activation='relu', recurrent_activation='sigmoid', kernel_initializer = initializers.lecun_normal(seed1)))(c_features)
    h_features1 = keras.layers.concatenate([c_features, g_features1], axis=-1)
    g_features2 = Bidirectional(GRU(128,return_sequences=True, activation='relu', recurrent_activation='sigmoid', kernel_initializer = initializers.lecun_normal(seed1)))(h_features1)
    all_f = keras.layers.concatenate([c_features, g_features2], axis=-1)
    all_f = Dropout(rate)(all_f)
    all_features = two_attention(all_f)

    global_features = keras.layers.concatenate([all_features, position_vectors], axis=-1) 
    global_features = Dropout(rate)(global_features)
    fc = Dense(128, activation='selu', kernel_regularizer=l2(L2_value), kernel_initializer = initializers.lecun_normal(seed1))(global_features)
    preds = Dense(1, activation='sigmoid', kernel_initializer = initializers.lecun_normal(seed1))(fc)
    adam = Adam(lr=0.0003, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
    AGNet = Model(inputs=sequence_input, outputs = preds)
    AGNet.compile(loss='binary_crossentropy', optimizer=adam, metrics=['accuracy'])
    AGNet.summary()
    return AGNet
