# AGNet
This is the source code for paper.


Attentive gated neural networks for identifying chromatin accessibility, Neural Computing and Applications (2020), https://doi.org/10.1007/s00521-020-04879-7.

In this work, to address the issues, we first encode the DNA sequences by using position embeddings, which are produced by integrating position information of the original sequences into embedding vectors and then propose a novel deep learning framework, called attentive gated neural networks (AGNet), to automatically extract complex patterns for predicting chromatin accessibility from DNA sequences. Specifically, we combine gated neural networks (GNNs) with dual attention to extract multiple patterns and long-term associations merely from DNA sequences. Experimental results on five cell-type datasets show that AGNet obtains the best performance than the published methods for the accessibility prediction. Furthermore, the results not only reveal that AGNet can learn more regulatory patterns that underlie DNA sequences, but also validate the significance of position embeddings for the accessibility prediction.

The code is mainly written in Python (3.5) using Keras (2.1.0) with tensorflow backend.



Data can be downloaded at 'https://github.com/minxueric/ismb2017_lstm.'.

