from numpy.random import seed
seed(2019)
from random import  seed
seed(2019)
from tensorflow import set_random_seed
set_random_seed(2019)    
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.layers import Dense, LSTM, Dropout, Bidirectional
from keras.layers import Convolution1D, MaxPooling1D
from keras.layers.embeddings import Embedding
from keras.callbacks import ModelCheckpoint, EarlyStopping
from keras.optimizers import Adam,RMSprop
import keras
from keras import backend as kb
import tensorflow as tf
from utils import *
import argparse
import gc
import time 
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from sklearn import metrics  
rate = 0.2
L2_value = 0.001                 
print ('drop rate|L2_value',rate,L2_value)
parser = argparse.ArgumentParser(description='embedcongru training')
parser.add_argument('-k', dest='k', type=int, default=5, help='length of kmer')
parser.add_argument('-s', dest='s', type=int, default=2, help='stride of splitting')
parser.add_argument('-batchsize', dest='batchsize', type=int, default = 320, help='size of one batch')
parser.add_argument('-model_name', dest='model_name', type=str, default='AGNet embedding', help='model name')
parser.add_argument('-init', dest='init', action='store_true', default=True, help='initialze vector')
parser.add_argument('-trainable', dest='trainable', action='store_true', default=True, help='embedding vectors trainable?')
parser.add_argument('-test', dest='test', action='store_true', default=False, help='only test step')
parser.add_argument('-patience', dest='patience', type=int, default= 5 , help='the patience to use')
parser.add_argument('-MAX_LEN', dest='MAX_LEN', type=int, default=1500, help='')
parser.add_argument('-embedding_vector_length', dest='embedding_vector_length', type=int, default=100, help='')
parser.add_argument('-epoch', dest='epoch', type=int, default=100, help='')
args = parser.parse_args()                    
# names = ['SNEDE0000EMT','SNEDE0000EPC', 'SNEDE0000EPH','SNEDE0000ENO', 'SNEDE0000EMU']
# names = ['SNEDE0000EMT','SNEDE0000EMU']
# names = ['SNEDE0000EPC'];
# names = ['SNEDE0000ENO'];
names = ['SNEDE0000EPH'];
# names = ['SNEDE0000EMT'];

for i in range(len(names)):
    name = names[i]
    print (name)
    print ('K:',args.k)
    print ('s:',args.s)
    print ('batchsize:',args.batchsize)
    print('init:',args.init)
    print('trainable:',args.trainable)
    print('dataset:',name)
    print('patience:',args.patience)
    print('model_name:',args.model_name)
    print('max_len:',args.MAX_LEN)
    print('embedding_vector_length',args.embedding_vector_length) 
    print ('Loading seq data...')
    pos_seqs = [line[:-2] for line in open('./data/%s_pos_%dgram_%dstride' % (name, args.k, args.s)) if len(line.split())>15]        
    neg_seqs = [line[:-2] for line in open('./data/%s_neg_%dgram_%dstride' % (name, args.k, args.s)) if len(line.split())>15]                
    seqs = pos_seqs + neg_seqs
    lens = [len(line.split()) for line in seqs]
    n_seqs = len(lens)                      
    y = np.array([1] * len(pos_seqs) + [0] * len(neg_seqs))        
    print ('Tokenizing seqs...')
    NB_WORDS = 20000
    tokenizer = Tokenizer(num_words=NB_WORDS) ##num_words: max word size    
    tokenizer.fit_on_texts(seqs)
    sequences = tokenizer.texts_to_sequences(seqs)
    X = pad_sequences(sequences, maxlen=args.MAX_LEN)        
    kmer_index = tokenizer.word_index
    print ('Found %s unique tokens.' % len(kmer_index))
    print ('Spliting train, valid, test parts...')        
    # fix random seeds to reduce variability of results
    np.random.seed(2019)
    indices = np.arange(n_seqs)
    np.random.shuffle(indices)
    X = X[indices]
    y = y[indices]        
    n_tr = int(n_seqs * 0.85)
    n_va = int(n_seqs * 0.05)
    n_te = n_seqs - n_tr - n_va    
    X_train = X[:n_tr]
    y_train = y[:n_tr]
    X_valid = X[n_tr:n_tr+n_va]
    y_valid = y[n_tr:n_tr+n_va]
    X_test = X[-n_te:]
    y_test = y[-n_te:]    
    nb_words = min(NB_WORDS, len(kmer_index)) # kmer_index starting from 1
    print('Building model...')
    print ('nb_words',nb_words)                 
    if args.init:
        print ('initialize embedding layer with glove vectors')
        kmer2vec={}
        f = open('./data/%s_%dgram_%dstride_vectors.txt' % (name, args.k, args.s))
        for line in f:
            values = line.split()
            try:
                kmer = values[0]
                coefs = np.asarray(values[1:], dtype='float32')
                kmer2vec[kmer] = coefs
            except:pass
        f.close()
        embedding_matrix = np.zeros((nb_words+1, args.embedding_vector_length))
        for kmer, i in kmer_index.items():
            if i > NB_WORDS:
                continue
            vector = kmer2vec.get(kmer)
            if vector is not None:
                embedding_matrix[i] = vector
    session_conf = tf.ConfigProto(intra_op_parallelism_threads=1, inter_op_parallelism_threads=1)                        
    sess = tf.Session(graph=tf.get_default_graph(), config=session_conf)
    kb.set_session(sess) 
    model = build_agnet(args,embedding_matrix,nb_words,rate,L2_value)

    if not args.test:
        checkpointer = ModelCheckpoint(monitor='val_loss',filepath="./model/%s_%s_%dgram_%dstride_drop%sL2_value%s.hdf5" \
                        % (name, args.model_name, args.k, args.s,rate,L2_value), verbose=1, save_best_only=True)
        earlystopper = EarlyStopping(monitor='val_loss', patience = 5, verbose=1)
        print ('Training model...')
        start = time.time()
        history = model.fit(X_train, y_train, epochs=100, batch_size=args.batchsize,
                    validation_data=(X_valid, y_valid),
                    callbacks=[checkpointer,earlystopper],
                    verbose=2)
        end = time.time()
        running_time = end-start
        print ('model training running_time', running_time)
        print ('End up training')
        # plotLoss(history,name,args.model_name)        
    print ('Testing model...')
    model.load_weights("./model/%s_%s_%dgram_%dstride_drop%sL2_value%s.hdf5" \
                        % (name, args.model_name, args.k, args.s,rate,L2_value))
    print ('Testing model and get loss and acc values')
    tresults = model.evaluate(X_test, y_test)
    print ('tresults',tresults)
    y_pred = model.predict(X_test, batch_size=args.batchsize, verbose=1)
    y = y_test
    print ('Calculating AUC...')
    auroc = metrics.roc_auc_score(y, y_pred)
    auprc = metrics.average_precision_score(y, y_pred)
    print ('auroc%s \t auprc%s '% (auroc, auprc))    
    kb.clear_session()
    del pos_seqs,neg_seqs,seqs,y,y_test,y_train,y_valid,y_pred,X,X_test,X_train,X_valid
    del tokenizer,sequences,kmer_index,n_tr,n_seqs,n_va,n_te,embedding_matrix
    gc.collect()
